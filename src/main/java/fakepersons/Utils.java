package fakepersons;

import fakepersons.data.InputItem;
import fakepersons.data.OutputItem;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Utils
{
    public static String PublishOutputs(OutputItem[] outputItems, String checkerName, Properties config)
            throws IOException, InterruptedException
    {
        String user = config.getProperty("user");
        if(user == null)
        {
            throw new IllegalArgumentException("user missing");
        }
        String group = config.getProperty("group");
        String baseUrl = config.getProperty("outputUrl");

        String[] resultItems = new String[outputItems.length];
        for(int i=0;i<outputItems.length;i++)
        {
            resultItems[i] = outputItems[i].getInputItemIndex()+","+outputItems[i].getMessage();
        }
        String body = String.join("\n",resultItems);

        String checkerName1 = URLEncoder.encode(checkerName, StandardCharsets.UTF_8.toString());
        String url = baseUrl + "?user=" + user + "&group="+group+"&checkerName="+checkerName1;
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }

    public static InputItem[] ReadInputFile() throws IOException
    {
        File inputFile=new File(Paths.get(System.getProperty("user.dir"),"data","input.csv").toString());
        List<String> lines = Files.readAllLines(inputFile.toPath(), StandardCharsets.UTF_8);
        List<InputItem> result = new ArrayList<>();
        for(String line : lines)
        {
            String[] data = line.split(";",-1);
            result.add(new InputItem(
                    data[0],
                    data[1],
                    data[2],
                    data[3],
                    data[4],
                    data[5],
                    data[6],
                    data[7]));
        }

        return result.toArray(new InputItem[0]);
    }

}
