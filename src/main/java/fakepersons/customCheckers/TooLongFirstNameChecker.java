package fakepersons.customCheckers;

import fakepersons.checkers.OneByOneChecker;
import fakepersons.data.InputItem;

public class TooLongFirstNameChecker extends OneByOneChecker
{
    public TooLongFirstNameChecker(int maximum)
    {
        this.maximum = maximum;
    }

    private int maximum;
    @Override
    public String GetCheckerName()
    {
        return "Křestní jméno";
    }

    public String CheckSingleItem(InputItem inputItem)
    {
        if(inputItem.getFirstName().length() > maximum)
        {
            return "Moc dlouhé ("+inputItem.getFirstName().length()+")";
        }
        return null;
    }

}
