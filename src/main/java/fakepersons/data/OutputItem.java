package fakepersons.data;

public  class OutputItem
{
    public OutputItem(int inputItemIndex, String message)
    {
        this.inputItemIndex = inputItemIndex;
        this.message = message;
    }

    private int inputItemIndex;

    private String message;

    public int getInputItemIndex()
    {
        return inputItemIndex;
    }

    public String getMessage()
    {
        return message;
    }
}
